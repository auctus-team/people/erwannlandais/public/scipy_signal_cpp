The installation process is a little bit complicated, but here's the main steps.

## Required Dependencies

### Pinocchio 

For now, the majority of the package is based on the use of the Pinocchio package to perform matrix transformations.

It can be downloaded from here :  https://stack-of-tasks.github.io/pinocchio/download.html . Do not forget to download the Python 3 version compatible of Pinocchio.

### OpenSim

Check the instructions inside the folder opensim_install.

### Final command

Add to ~/.bashrc the following lines : 

~~~
### For scipy_signal librairies
export SIGNAL_PATH=<your_path>

export PATH=$SCIPY_SIGNAL_PATH/lib:$PATH
export LIBRARY_PATH=$HOME/catkin_ws/devel/lib:$LIBRARY_PATH
export LD_LIBRARY_PATH=$HOME/catkin_ws/devel/lib:$LD_LIBRARY_PATH
~~~

And do : 

~~~
source ~/.bashrc
sudo ldconfig
~~~

This may be necessary for Cmake to find the libraries. There might be a better solution, but I haven't found it yet.

## Import pyosim into an other project 

(from https://docs.ros.org/en/api/catkin/html/howto/format2/installing_python.html)

You can do it with a CMakeLists.txt, into a catkin package.
Just add the following line into your CMakeLists.txt : 

~~~
catkin_python_setup()
~~~

Then, build your package, source devel/setup.bash, and you will have access to the pyosim module : 

~~~
from pyosim.OsimMode import OsimModel 
~~~

A example is given into this package with the demo_pyosim example.
