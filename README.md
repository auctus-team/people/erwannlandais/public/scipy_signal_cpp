# human_library

Components of the package : 

- pyosim, a Python library for a quick access to useful functions of Opensim (get/set joint configurations, get markers configurations, Inverse Kinematics operations, ... ).
(Warning : to be called, you must ask for pyosim.OsimModel; in addition, you will need to source catkin_ws/devel/setup.bash).

- fpin : a library for a quick access to useful functions of Pinocchio (set joint / speed configurations, forward Kinematics).

- scipy_signal : a really small library, implementing some functions of the scipy.signal module into C++ (butterworth, filtfilt only).

Furthermore, this library can be used together with the ospi2urdf package, in order to save the generated .urdf files from the .osim files. 

An executable is also given (q_pub), which moves the joints of the model over their entire positional range and republish the configuration of the model (through the topic /Q_pub).

An other executable (demo_pyopsim) is also given to test the install of the python library pyopsim. If everything is all right, a list of int should be displayed as a return of this executable.
