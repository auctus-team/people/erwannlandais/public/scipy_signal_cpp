#include "scipy_signal_cpp/scipy_signal.h"
#include "PolyfitEigen.hpp"
#include <cstdlib>
#include <time.h>
#include <random>
#include <map>

#include <libIntegrate/Integrate.hpp>

namespace signal_utilities
{

    void completeSecondOrderDerivation(
        std::map<std::string,std::vector< std::vector<double> > > zeroOrder,
        std::map<std::string,std::vector< std::vector<double> > > & zeroOrderFiltered,
        std::map<std::string,std::vector< std::vector<double> > > & oneOrder,
        std::map<std::string,std::vector< std::vector<double> > > & oneOrderFiltered,
        std::map<std::string,std::vector< std::vector<double> > > & twoOrder,
        std::map<std::string,std::vector< std::vector<double> > > & twoOrderFiltered,
        std::vector<double> doubleTime,
        Eigen::MatrixXd a,
        Eigen::MatrixXd b, 
        int padlen,
        int polyDeg,
        int numFramesAfterOri,
        int numFramesInFuture,
        std::vector<int> v_nq,
        std::string filteringMode
    );

    void completeSecondOrderIntegration(
        std::map<std::string,std::vector< std::vector<double> > > twoOrder,
        std::map<std::string,std::vector< std::vector<double> > > & oneOrder,
        std::map<std::string,std::vector< std::vector<double> > > & zeroOrder,
        std::vector<int> v_nq        
    );        

    void checkAndAddValue(std::vector<std::vector<double> > & v, 
    int dim,
    int i,
    int j,
    double value
    );

    void butterworthManagement(std::vector<std::vector<double> > rawValues,
    Eigen::MatrixXd a,
    Eigen::MatrixXd b,   
    int padlen, 
    int jointDof,    
    std::vector<std::vector<double> > & filteredValues);

    void polyfitOperation(std::vector<std::vector<double>> rawValues,
    std::vector<double> doubleTime,
    int polyDeg,
    int jointDof,  
    std::vector< std::vector<double> > & derivedValues,
    std::vector<double> & coefficients);

    void finiteDifferenceComputation(
        std::vector<std::vector<double>> rawValues,
        std::vector<double> prev_times,
        int numFramesAfterOri,
        int numFramesInFuture,        
        int jointDof,
        int nv,
        std::vector<std::vector<double> > & derivedValues       
        );

  double OneEuroFilter(bool oneEuroFirstTime, double x,  double rate, double fcmin,
  double beta,  double dcutoff, double & hatxprev, double & dhatxprev );

  void lowPassFilter(bool oneEuroFirstTime, double x, double alpha, double & hatxprev);


    double alphaCompute(double rate, double cutoff);        
    
}