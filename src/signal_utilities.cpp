#include "scipy_signal_cpp/signal_utilities.h"

namespace signal_utilities
{

    /**
     * @brief 
     * 
     * @param zeroOrder => nameOfTheObjectToBeFiltered : WindowWithTheDataAboutThisObject
     * WindowWithTheDataAboutThisObject[k] : values of the data about the object at time k. Those data
     * could be positions (x-y-z) in meter.
     * Ex : zeroOrder = [
     *                      Position : [ [x0, y0, z0] , [x1, y1, z1], ... [xn, yn, zn] ]
     *                      Orientation : [ [qx0, qy0, qz0, qw0], [qx1, qy1, qz1, qw1], ... [qxn, qyn, qzn, qwn] ]
     *                  ]
     * 
     * @param zeroOrderFiltered 
     * @param oneOrder 
     * @param oneOrderFiltered 
     * @param twoOrder 
     * @param twoOrderFiltered 
     * @param doubleTime 
     * @param a 
     * @param b 
     * @param padlen 
     * @param polyDeg 
     * @param numFramesAfterOri 
     * @param numFramesInFuture 
     * @param v_nq : parameters 
     * @param filteringMode : 3 types : polyfit, butterworth, none
     */
    void completeSecondOrderDerivation(
        std::map<std::string,std::vector< std::vector<double> > > zeroOrder,
        std::map<std::string,std::vector< std::vector<double> > > & zeroOrderFiltered,
        std::map<std::string,std::vector< std::vector<double> > > & oneOrder,
        std::map<std::string,std::vector< std::vector<double> > > & oneOrderFiltered,
        std::map<std::string,std::vector< std::vector<double> > > & twoOrder,
        std::map<std::string,std::vector< std::vector<double> > > & twoOrderFiltered,
        std::vector<double> doubleTime,
        Eigen::MatrixXd a,
        Eigen::MatrixXd b, 
        int padlen,
        int polyDeg,
        int numFramesAfterOri,
        int numFramesInFuture,
        std::vector<int> v_nq,
        std::string filteringMode
    )
    {

        // reboot all necessary variables

        // (to be sure that filt and raw will have the same size)
        zeroOrderFiltered = zeroOrder;
        std::map<std::string,std::vector< std::vector<double> > > zeroOrderButter = zeroOrder;
        int i_nq = 0;        

        for (std::map<std::string,std::vector< std::vector<double> > >::iterator it = zeroOrder.begin(); it!= zeroOrder.end(); it++)
        {
            std::string nameJoint = it->first;
            int jointSpaceSize = it->second[0].size();
            int nv = v_nq[i_nq];
            i_nq++;

            for (int l = 0; l < jointSpaceSize; l++)
            {
                if (filteringMode=="polyfit")
                {

                    // first, compute the filtered values (ex : with butterworth)

                    butterworthManagement(zeroOrder[nameJoint],a,b,padlen,l,zeroOrderButter[nameJoint]);

                    // execute a polyfit operation (in order to get at the same time value and speed)                    
                    std::vector<double> coefficients;
                    polyfitOperation(zeroOrderButter[nameJoint],doubleTime,polyDeg,l,zeroOrderFiltered[nameJoint],coefficients);
                    if (coefficients.size() != 0)
                    {
                        //std::cout << "add with poly" << std::endl;
                        // get speed 
                        if (coefficients.size() >1)
                        {
                            //std::cout << nv << std::endl;
                            for (int i = numFramesAfterOri; i < doubleTime.size() -numFramesInFuture; i++)
                            {
                                double deriv_value = coefficients[1];                
                                for (int j = 2; j < coefficients.size(); j++)
                                {
                                    deriv_value += j*coefficients[j]*pow(doubleTime[i], j-1);
                                }   
         
                                checkAndAddValue(oneOrder[nameJoint],nv,i-numFramesAfterOri,l,deriv_value);

                            }            
                            // filtrate speed
                            //oneOrderFiltered[nameJoint].clear();
                            oneOrderFiltered[nameJoint] = oneOrder[nameJoint];
                            //std::cout << oneOrderFiltered[nameJoint].size() << std::endl;

                        }

                        // get acceleration
                        if (coefficients.size() > 2)
                        {
                            for (int i = 2*numFramesAfterOri; i < doubleTime.size()-2*numFramesInFuture ; i++)
                            {
                                double deriv_value = 2*coefficients[2];                
                                for (int j = 3; j < coefficients.size(); j++)
                                {
                                    deriv_value += j*(j-1)*coefficients[j]*pow(doubleTime[i], j-2);
                                }                
                                //std::cout << "bef acc : " << twoOrder[nameJoint].size() << std::endl;
                                checkAndAddValue(twoOrder[nameJoint],nv,i-2*numFramesAfterOri,l,deriv_value);
                                //std::cout << "aft acc : " << twoOrder[nameJoint].size() << std::endl;
                            }
                            // then, filtrate acceleration
                            twoOrderFiltered[nameJoint] = twoOrder[nameJoint];   
                        }

                    }
                    else
                    {
                        filteringMode == "none";
                    }
                    
                }
                if (filteringMode == "butterworth")
                {
                    // taken from "Filtering Biomechanical Signals in Movement Analysis"
                    // https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8271607/
                    // first, compute the filtered values (with butterworth)
                    //std::cout << zeroOrder[nameJoint].size() << std::endl;        
                    butterworthManagement(zeroOrder[nameJoint],a,b,padlen,l,zeroOrderFiltered[nameJoint]);
                    //std::cout << zeroOrderFiltered[nameJoint].size() << std::endl;        
                    finiteDifferenceComputation(zeroOrderFiltered[nameJoint], doubleTime, numFramesAfterOri, numFramesInFuture, l, nv, oneOrder[nameJoint]);
                    //std::cout << oneOrder[nameJoint].size() << std::endl;     
                    std::vector<double> oneOrderTime(doubleTime.begin()+numFramesAfterOri, doubleTime.end()- numFramesInFuture);
                    // then, filtrate speed
                    oneOrderFiltered[nameJoint] = oneOrder[nameJoint];
                    butterworthManagement(oneOrder[nameJoint],a,b,padlen,l,oneOrderFiltered[nameJoint]);
                    //std::cout << oneOrderFiltered[nameJoint].size() << std::endl;                       
                    // compute acceleration
                    finiteDifferenceComputation(oneOrderFiltered[nameJoint], oneOrderTime, numFramesAfterOri, numFramesInFuture, l, nv, twoOrder[nameJoint]);                 
                    //std::cout << twoOrder[nameJoint].size() << std::endl;       
                    // then, filtrate acceleration
                    twoOrderFiltered[nameJoint] = twoOrder[nameJoint];
                    butterworthManagement(twoOrder[nameJoint],a,b,padlen,l,twoOrderFiltered[nameJoint]);
                    //std::cout << twoOrderFiltered[nameJoint].size() << std::endl;   
                }

                if (filteringMode == "none")
                {
                    //std::cout << "FDC activated" << std::endl;
                    zeroOrderFiltered[nameJoint] = zeroOrder[nameJoint];
                    // compute speed 
                    finiteDifferenceComputation(zeroOrderFiltered[nameJoint], doubleTime, numFramesAfterOri, numFramesInFuture, l, nv, oneOrder[nameJoint]);
                    std::vector<double> oneOrderTime(doubleTime.begin()+numFramesAfterOri, doubleTime.end()- numFramesInFuture);
                    oneOrderFiltered[nameJoint] = oneOrder[nameJoint];
                    //std::cout << oneOrderFiltered[nameJoint].size() << std::endl;
                    // compute acceleration
                    finiteDifferenceComputation(oneOrderFiltered[nameJoint], oneOrderTime, numFramesAfterOri, numFramesInFuture, l, nv, twoOrder[nameJoint]);
                    twoOrderFiltered[nameJoint] = twoOrder[nameJoint];   

                }


            }

        }        

    }
    /**
     * @brief 
     * 
     * @param v 
     * @param dim 
     * @param i 
     * @param j 
     * @param value 
     */
    void checkAndAddValue(std::vector<std::vector<double> > & v, 
    int dim,
    int i,
    int j,
    double value
    )
    {
        while (v.size() < i+1)
        {
            std::vector<double> vals(dim,0.0);
            v.push_back(vals);
        }
        v[i][j] = value;
    }

    /**
     * @brief Apply a butterworth Filter (defined by the coefficient a and b) on the
     * l dimension of the rawValues datas.
     * 
     * @param rawValues 
     * @param a 
     * @param b 
     * @param padlen 
     * @param jointDof 
     * @param filteredValues 
     */
    void butterworthManagement(std::vector<std::vector<double> > rawValues,
    Eigen::MatrixXd a,
    Eigen::MatrixXd b,   
    int padlen, 
    int jointDof,    
    std::vector<std::vector<double> > & filteredValues)
    {
        int l = jointDof;

        Eigen::MatrixXd rawVals(rawValues.size(),1);
        Eigen::MatrixXd filteredVals;
        for (int k = 0; k < rawValues.size(); k++)
        {
            rawVals(k,0) =  rawValues[k][l];
        }
        int result = scipy_signal::filtfilt(b,a,rawVals,filteredVals,0,"odd",padlen);

        for (int k = 0; k < filteredValues.size(); k++)
        {
            filteredValues[k][l] = filteredVals(k,0);
        }
    }


    /**
     * @brief Returns no coefficients if there is an issue with the polyfit operation.
     * 
     * @param rawValues 
     * @param doubleTime 
     * @param polyDeg 
     * @param jointDof 
     * @param filteredValues 
     * @param coefficients 
     */
    void polyfitOperation(std::vector<std::vector<double>> rawValues,
    std::vector<double> doubleTime,
    int polyDeg,
    int jointDof,    
    std::vector< std::vector<double> > & filteredValues,
    std::vector<double> & coefficients
    )
    {
        std::vector<double> rVals;
        for (int i = 0; i < rawValues.size(); i++)
        {
            rVals.push_back(rawValues[i][jointDof]);
        }        

        coefficients = polyfit_Eigen(doubleTime, rVals, polyDeg);

        double sum = 0;
        bool coeffsOk = true;

        int l = 0;
        while (coeffsOk && l < coefficients.size())
        {
            if (isnan(coefficients[l]) || isinf(coefficients[l]) )
            {
                coeffsOk = false;
            }
            else
            {
                //std::cout << "coefficient" << l << " : " << coefficients[l] << std::endl;
                sum += coefficients[l];
            }
            l++;
            
        }

        coeffsOk = (coeffsOk && (abs(sum) > 1e-12) );

        if (coeffsOk)
        {
            // filter
            for (int i = 0; i < doubleTime.size(); i++ )
            {
                double value = coefficients[0];

                for (int j = 1; j < coefficients.size(); j++)
                {
                    value += coefficients[j]*pow(doubleTime[i], j);
                }
                filteredValues[i][jointDof] = (value);
            }
        }

        else
        {
            // error : returns an empty coefficients vector
            coefficients.clear();
        }
    }

    void finiteDifferenceComputation(
        std::vector<std::vector<double>> rawValues,
        std::vector<double> prev_times,
        int numFramesAfterOri,
        int numFramesInFuture,        
        int jointDof,
        int nv,
        std::vector<std::vector<double> > & derivedValues       
        )
    {
        int l = jointDof;

        for (int i = numFramesAfterOri; i < rawValues.size() -numFramesInFuture; i++)
        {
            double duration = (prev_times[i-1]- prev_times[i]);
            double v1 = rawValues[i-2][l];
            double v2 = rawValues[i-1][l];
            double v3 = rawValues[i][l];
            double v4 = rawValues[i+1][l];
            double v5 = rawValues[i+2][l];            

            double deriv = (-v5 + 8*v4 - 8*v2+ + v1 )/(12*duration);

            checkAndAddValue(derivedValues,nv,i - numFramesAfterOri ,l,deriv);            

        }
    }

  double OneEuroFilter(bool oneEuroFirstTime, double x,  double rate, double fcmin,
  double beta,  double dcutoff, double & hatxprev, double & dhatxprev )
  {
      double dx;
      if (oneEuroFirstTime)
      {
          dx = 0;
      }
      else
      {
          dx = (x-hatxprev)*rate;
      }

      double edx;
      lowPassFilter(oneEuroFirstTime, dx, alphaCompute(rate,dcutoff) , dhatxprev);
      edx = dhatxprev;

      double cutoff = fcmin+ beta*abs(edx);

      double result;
      lowPassFilter(oneEuroFirstTime, x, alphaCompute(rate,cutoff),result);


      if (oneEuroFirstTime)
      {
          oneEuroFirstTime = false;
      }

      return(result);


  }    

  void lowPassFilter(bool oneEuroFirstTime, double x, double alpha, double & hatxprev)
  {
      if (oneEuroFirstTime)
      {
          hatxprev = x;
      }
      hatxprev = alpha*x + (1-alpha)*hatxprev;
  }

  double alphaCompute(double rate, double cutoff)
  {
      double tau = 1.0/(2*M_PI*cutoff);
      double te = 1.0/rate;
      return(1.0/(1.0+ tau/te) );
  }


  void completeSecondOrderIntegration(
        std::map<std::string,std::vector< std::vector<double> > > twoOrder,
        std::map<std::string,std::vector< std::vector<double> > > & oneOrder,
        std::map<std::string,std::vector< std::vector<double> > > & zeroOrder,
        std::vector<int> v_nq
  )
  {

    // int i_nq = 0;        

    // for (std::map<std::string,std::vector< std::vector<double> > >::iterator it = twoOrder.begin(); it!= twoOrder.end(); it++)
    // {
    //     std::string nameJoint = it->first;
    //     int jointSpaceSize = it->second[0].size();
    //     int nv = v_nq[i_nq];
    //     i_nq++;
    //     for (int l = 0; l < jointSpaceSize; l++)
    //     {

    // }
  }


       
    
    
}