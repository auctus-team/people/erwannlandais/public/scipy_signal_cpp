#include <cstdlib>
#include <time.h>
#include "PolyfitEigen.hpp"
#include <sstream>
#include <iostream>
#include <cmath>
#include <map>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/Dense>
#include <vector>
#include <string>
#include <complex>

    /**
     * @brief Returns no coefficients if there is an issue with the polyfit operation.
     * 
     * @param rawValues 
     * @param doubleTime 
     * @param polyDeg 
     * @param jointDof 
     * @param filteredValues 
     * @param coefficients 
     */
    void polyfitOperation(std::vector<std::vector<double>> rawValues,
    std::vector<double> doubleTime,
    int polyDeg,
    int jointDof,    
    std::vector< std::vector<double> > & filteredValues,
    std::vector<double> & coefficients
    )
    {
        std::vector<double> rVals;
        for (int i = 0; i < rawValues.size(); i++)
        {
            rVals.push_back(rawValues[i][jointDof]);
        }        

        coefficients = polyfit_Eigen(doubleTime, rVals, polyDeg);

        double sum = 0;
        bool coeffsOk = true;

        int l = 0;
        while (coeffsOk && l < coefficients.size())
        {
            if (isnan(coefficients[l]) || isinf(coefficients[l]) )
            {
                coeffsOk = false;
            }
            else
            {
                //std::cout << "coefficient" << l << " : " << coefficients[l] << std::endl;
                sum += coefficients[l];
            }
            l++;
            
        }

        coeffsOk = (coeffsOk && (abs(sum) > 1e-12) );

        if (coeffsOk)
        {
            // filter
            for (int i = 0; i < doubleTime.size(); i++ )
            {
                double value = coefficients[0];

                for (int j = 1; j < coefficients.size(); j++)
                {
                    value += coefficients[j]*pow(doubleTime[i], j);
                }
                filteredValues[i][jointDof] = (value);
            }
        }

        else
        {
            // error : returns an empty coefficients vector
            coefficients.clear();
        }
    }